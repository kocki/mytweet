# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.mail import send_mail
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Account(AbstractBaseUser, PermissionsMixin):
    """
    Used django auth with some extensions.
    """
    email = models.EmailField(_('email address'), unique=True)
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    is_active = models.BooleanField(_('active'), default=True)

    # here we collect favorites tweet and account to follow
    favorites = models.ManyToManyField(
        'Tweet', blank=True, related_name='favoring')
    followers = models.ManyToManyField(
        'self', blank=True, through='Follow',
        symmetrical=False)


    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        '''
        send_mail(subject, message, from_email, [self.email], **kwargs)


class Tweet(models.Model):
    account = models.ForeignKey(Account)
    # don't allow empty tweet
    content = models.CharField(max_length=250)
    # retweets which work in tree-structure (we can use django-mptt to make it
    # faster and more flexible
    date = models.DateTimeField(auto_now_add=True)
    reply_to = models.ForeignKey(
        'self', blank=True, null=True, related_name='retweets')


class Follow(models.Model):
    tweet = models.ForeignKey(Tweet)
    account = models.ForeignKey(Account)
    order = models.IntegerField(default=0)


class Mention(models.Model):
    # who sent:
    sender = models.ForeignKey(Account, related_name='mention_senders')
    # who receive:
    receivers = models.ManyToManyField(
        Account, related_name='mention_receivers')
    tweet = models.ForeignKey(Tweet)
    date = models.DateTimeField(auto_now_add=True)
